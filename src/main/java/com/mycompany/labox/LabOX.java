/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.labox;

/**
 *
 * @author user
 */

import java.util.Scanner;

public class LabOX {
    
    static char [][] Board = {{'-','-','-'},
                              {'-','-','-'},
                              {'-','-','-'}};

    static char currentPlayer = 'O';
    static String inputPos;
    static int count = 0;
    
    public static void main(String[] args) {
        
        printWelcome();
        exPosition();

        while(true){
            printBoard();
            printTurn();
            inputPos();
            
            if(cheakWin()){
                printBoard();
                printWin();
                break;
            }
            
            if(cheakDraw()){
                printBoard();
                printDraw();
                break;
            }
            
            swtichPlayer();
        }
    }

    private static void printWelcome() {
        System.out.println("Welcome to OX game!");
    }
    
    private static void exPosition() {
        System.out.println("1 " + "2 " + "3");
        System.out.println("4 " + "5 " + "6");
        System.out.println("7 " + "8 " + "9");
    }

    private static void printBoard() {
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                System.out.print(Board[i][j]+ " ");
            }
            System.out.println("");
        
        }
    }
    
    private static void printTurn() {
        System.out.println("turn "+ currentPlayer);
    }
    
    private static void inputPos() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input position(1-9) : ");
        inputPos = sc.next();
        switch(inputPos){
            case "1":
                Board[0][0] = currentPlayer;
                break;
            case "2":
                Board[0][1] = currentPlayer;
                break;
            case "3":
                Board[0][2] = currentPlayer;
                break;
            case "4":
                Board[1][0] = currentPlayer;
                break;
            case "5":
                Board[1][1] = currentPlayer;
                break;
            case "6":
                Board[1][2] = currentPlayer;
                break;
            case "7":
                Board[2][0] = currentPlayer;
                break;
            case "8":
                Board[2][1] = currentPlayer;
                break;
            case "9":
                Board[2][2] = currentPlayer;
                break;
            default:
                System.out.println("!!!Please Input only(1-9)!!!");
        }
        count++;
    }
    
    private static void swtichPlayer() {
        if(currentPlayer == 'O'){
            currentPlayer = 'X';
        }else{
            currentPlayer = 'O';
        }
    }
    
    
    private static boolean cheakWin() {
        if(cheakRow()){
            return true;     
        }else if(cheakCol()){
            return true; 
        }else if(cheakCross1()){
            return true; 
        }else if(cheakCross2()){
            return true; 
        }
        return false;
    }
    
    private static void printWin() {
        System.out.println("Congratulation! Player "+currentPlayer+" win!");
    }
    
    private static boolean cheakRow() {
        if((Board[0][0] == currentPlayer && Board[0][1] == currentPlayer && Board[0][2] == currentPlayer)||
           (Board[1][0] == currentPlayer && Board[1][1] == currentPlayer && Board[1][2] == currentPlayer)||
           (Board[2][0] == currentPlayer && Board[2][1] == currentPlayer && Board[2][2] == currentPlayer)){
            return true;
        }
        return false;
    }
    
    private static boolean cheakCol() {
        if((Board[0][0] == currentPlayer && Board[1][0] == currentPlayer && Board[2][0] == currentPlayer)||
           (Board[0][1] == currentPlayer && Board[1][1] == currentPlayer && Board[2][1] == currentPlayer)||
           (Board[0][2] == currentPlayer && Board[1][2] == currentPlayer && Board[2][2] == currentPlayer)){
            return true;
        }
        return false;
    }
    
    private static boolean cheakCross1() {
        if(Board[0][0] == currentPlayer && Board[1][1] == currentPlayer && Board[2][2] == currentPlayer){
            return true;
        }
        return false;
    }
    
    private static boolean cheakCross2() {
        if(Board[0][2] == currentPlayer && Board[1][1] == currentPlayer && Board[2][0] == currentPlayer){
            return true;
        }
        return false;
    }
   
    private static boolean cheakDraw() {
        if(count==9){
            return true;
        }
        return false;
    }
    
    private static void printDraw() {
        System.out.println("The game ended in the draw!");
    }
    
}
