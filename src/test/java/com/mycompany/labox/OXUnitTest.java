/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.labox;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void checkWin_O_Row1_output_true(){
        char[][] GameBoard = {{'O','O','O'},{'-','-','-'},{'-','-','-'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_O_Row2_output_true(){
        char[][] GameBoard = {{'-','-','-'},{'O','O','O'},{'-','-','-'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);      
    }
    
    @Test
    public void checkWin_O_Row3_output_true(){
        char[][] GameBoard = {{'-','-','-'},{'-','-','-'},{'O','O','O'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);      
    }
    
    @Test
    public void checkWin_O_Col1_output_true(){
        char[][] GameBoard = {{'O','-','-'},{'O','-','-'},{'O','-','-'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_O_Col2_output_true(){
        char[][] GameBoard = {{'-','O','-'},{'-','O','-'},{'-','O','-'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_O_Col3_output_true(){
        char[][] GameBoard = {{'-','-','O'},{'-','-','O'},{'-','-','O'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_X_Row1_output_true(){
        char[][] GameBoard = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_X_Row2_output_true(){
        char[][] GameBoard = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);      
    }
    
    @Test
    public void checkWin_X_Row3_output_true(){
        char[][] GameBoard = {{'-','-','-'},{'-','-','-'},{'X','X','X'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);      
    }
    
    @Test
    public void checkWin_X_Col1_output_true(){
        char[][] GameBoard = {{'X','-','-'},{'X','-','-'},{'X','-','-'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_X_Col2_output_true(){
        char[][] GameBoard = {{'-','X','-'},{'-','X','-'},{'-','X','-'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_X_Col3_output_true(){
        char[][] GameBoard = {{'-','-','X'},{'-','-','X'},{'-','-','X'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_O_cross1_output_true(){
        char[][] GameBoard = {{'O','-','-'},{'-','O','-'},{'-','-','O'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_O_cross2_output_true(){
        char[][] GameBoard = {{'-','-','O'},{'-','O','-'},{'O','-','-'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_X_cross1_output_true(){
        char[][] GameBoard = {{'X','-','-'},{'-','X','-'},{'-','-','X'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_X_cross2_output_true(){
        char[][] GameBoard = {{'-','-','X'},{'-','X','-'},{'X','-','-'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkDraw_O_output_true(){
        char[][] GameBoard = {{'O','O','-'},{'-','-','O'},{'O','O','-'}};
        char currentPlayer = 'O';
        boolean result = OX.checkDraw(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkDraw_X_output_true(){
        char[][] GameBoard = {{'-','-','X'},{'X','X','-'},{'-','-','X'}};
        char currentPlayer = 'O';
        boolean result = OX.checkDraw(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
}
